This is working repository for my dissertation for the purpose of easy, immediate sharing of the latest versions.

Once it is complete, accepted and submitted through formal channels, this repo will either simply mirror the official document site or be taken down.

## To my committee

### To access the pdf
Click the document link, and click download in the top right-rand corner of the preview pane.

### Only bibliography or equation spill changes
I have gotten rid of the worst cases but will update the document *only* with these types of changes --- content and page numbering etc should stay the same.

### except if you request changes
The only other changes I would make are at the request of one of my committee members.

